function calculateSum(n, cb){
    let sum = 0;
    for(let i = 0; i < n; i++){
        sum += i;

   }
   console.log(`the sum ${sum}`);
   cb(sum);
}

let n = 10;
calculateSum(n, function (sum) {
    // body...
    console.log(`Average : ${sum/n}`);
})

//simple async function

const message = function () {
    // body...
    console.log(`hello,  callback`);
}
setTimeout(message, 3000);

// anonomous async function
setTimeout(() => {
    console.log(`hello, 3 sec`);
}, 3000);

// function doStep1(init){
//     return init + 1;
// }

// function doStep2(init){
//     return init + 2;
// }

// function doStep3(init){
//     return init + 3;
// }

// function doOperations() {
//     // body...
//     let result = 0;
//     result = doStep1(result);
//     result = doStep2(result);
//     result = doStep3(result);
//     console.log(`result: ${result}`);
// }

// doOperations();

function doStep1(init, callback) {
    // body...
    const result = init + 1;
    callback(result);
}

function doStep2(init, callback) {
    // body...
    const result = init + 2;
    callback(result);
}

function doStep3(init, callback) {
    // body...
    const result = init + 3;
    callback(result);
}

function doOperation() {
    // body...
    doStep1(0, result1 =>{
        doStep2(result1, result2 =>{
            doStep3(result2, result3 =>{
                console.log(`result: ${result3}`);
            });
        });
    });
}

doOperation();
