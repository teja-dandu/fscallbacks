const fs = require('fs').promises;
// writing the file from fs module
const path = require('path');

const RUNSCONCEDED_BYTEAM = path.resolve(__dirname, "./runsConcededByTeam.json");
const MATCHES_PER_YEAR = path.resolve(__dirname, "./matchesPerYear.json");

async function readFile(filePath) {
  try {
    const data = await fs.readFile(filePath);
    console.log(data.toString());
  } catch (error) {
    console.error(`Got an error trying to read the file: ${error.message}`);
  }
}

readFile(RUNSCONCEDED_BYTEAM, () => {
  readFile(MATCHES_PER_YEAR, () => {
    console.log('read the matchesPerYear file after reading the runsConcededByTeam file')
  });
  // console.log("read the runsConcededByTeam file")
});


// (async function () {
//   await openFile();
// })();


//deleting the file using asynchronous
// const fs = require('fs').promises;

async function deleteFile(filePath) {
  try {
    await fs.unlink(filePath);
    console.log(`Deleted ${filePath}`);
  } catch (error) {
    console.error(`Got an error trying to delete the file: ${error.message}`);
  }
}

deleteFile(RUNSCONCEDED_BYTEAM, () => {
  deleteFile(MATCHES_PER_YEAR, () => {
    console.log('deleting the matchesPerYear file after deleting the runsConcededByTeam file');
  });

});

// (async function () {
//   await deleteFile();
// })();
